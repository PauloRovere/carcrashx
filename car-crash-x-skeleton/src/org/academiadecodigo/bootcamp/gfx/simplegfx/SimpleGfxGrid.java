package org.academiadecodigo.bootcamp.gfx.simplegfx;

import org.academiadecodigo.bootcamp.grid.Grid;
import org.academiadecodigo.bootcamp.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Color;

public class SimpleGfxGrid implements Grid {

    public static final int PADDING = 10;
    private int columns;
    private int rows;
    private Rectangle rectangle;
    private int cellSize;


    public SimpleGfxGrid(int cols, int rows){ //*****
        this.columns = cols;
        this.rows = rows;
       // this.cellSize = 10;
    }

    /**
     * @see Grid#init()
     */
    @Override
    public void init() { //*****
        //criar rectangle pad pad colToX rowToY
       rectangle = new Rectangle(PADDING, PADDING,columnToX(getCols()),rowToY(getRows()));
       rectangle.setColor(Color.BLUE);
       rectangle.draw();
    }

    /**
     * @see Grid#getCols()
     */
    @Override
    public int getCols() {
        return columns;
        //throw new UnsupportedOperationException();
    }

    /**
     * @see Grid#getRows()
     */
    @Override
    public int getRows() {
       return rows;
       // throw new UnsupportedOperationException();
    }

    /**
     * Obtains the width of the grid in pixels
     * @return the width of the grid
     */
    public int getWidth() {
       // return rectangle.getY();
        return rectangle.getWidth();
        //throw new UnsupportedOperationException();
    }

    /**
     * Obtains the height of the grid in pixels
     * @return the height of the grid
     */
    public int getHeight() {
       // return rectangle.getX();
        return rectangle.getHeight();
    }

    /**
     * Obtains the grid X position in the SimpleGFX canvas
     * @return the x position of the grid
     */
    public int getX() {
        //return columnToX(columns);
        return rectangle.getX();
        //throw new UnsupportedOperationException(); // ???
    }

    /**
     * Obtains the grid Y position in the SimpleGFX canvas
     * @return the y position of the grid
     */
    public int getY() {
        //return rowToY(rows);
        return rectangle.getY();
        //throw new UnsupportedOperationException(); // ???
    }

    /**
     * Obtains the pixel width and height of a grid position
     * @return
     */
    public int getCellSize() {
        return PADDING;

    }

    /**
     * @see Grid#makeGridPosition()
     */
    @Override
    public GridPosition makeGridPosition() {
        return new SimpleGfxGridPosition(this);

        }

    /**
     * @see Grid#makeGridPosition(int, int)
     */
    @Override
    public GridPosition makeGridPosition(int col, int row) {

        return new SimpleGfxGridPosition(col, row, this);

    }

    /**
     * Auxiliary method to compute the y value that corresponds to a specific row
     * @param row index
     * @return y pixel value
     */
    public int rowToY(int row) {
        //rows = row * cellSize;
        return (row * PADDING) ;
        //return rows;
    }

    /**
     * Auxiliary method to compute the x value that corresponds to a specific column
     * @param column index
     * @return x pixel value
     */
    public int columnToX(int column) {
        //columns = column * cellSize;
        return  (column * PADDING);
        //return columns;
    }
}
